package com.NanLabs.EjericioNanLabs.Controller;

import com.NanLabs.EjericioNanLabs.Service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TaskController {

    @Autowired
    TaskService taskService;

    @PostMapping("/crearTarea")
    public ResponseEntity<?> crearTarea(@RequestParam String typeCard, @RequestParam String title, @RequestParam String descipcion){

        taskService.crearCard(typeCard, title, descipcion);

        return ResponseEntity.ok("tarea creada");
    }
}
