package com.NanLabs.EjericioNanLabs.Service;

import com.NanLabs.EjericioNanLabs.Utils.AppConstants;
import kong.unirest.Unirest;
import org.springframework.stereotype.Service;

@Service
public class TaskService {

    public void crearCard(String typeCard, String title, String descripcion) {
        switch (typeCard) {
            case "issue" -> this.issueCard(title, descripcion);
            case "bug" -> this.bugCard(title, descripcion);
            case "task"  -> this.taskCard(title, descripcion);
        }
    }

    public void issueCard(String title, String descripcion) {
        Unirest.post("https://api.trello.com/1/cards")
                .queryString("key", AppConstants.key)
                .queryString("token", AppConstants.token)
                .queryString("idList", AppConstants.listaToDo)
                .queryString("name", title)
                .queryString("desc", descripcion)
                .asString();
    }

    public void bugCard(String title, String descripcion){
        int numeroRandom = (int) Math.floor(Math.random()*6+1);
        Unirest.post("https://api.trello.com/1/cards")
                .queryString("key", AppConstants.key)
                .queryString("token", AppConstants.token)
                .queryString("idList", AppConstants.listaDoing)
                .queryString("idLabels", AppConstants.labelBug)
                .queryString("name", "bug-" + title + "-" + numeroRandom)
                .queryString("desc", descripcion)
                .asString();
    }

    public void taskCard(String title, String descripcion){
        Unirest.post("https://api.trello.com/1/cards")
                .queryString("key", AppConstants.key)
                .queryString("token", AppConstants.token)
                .queryString("idList", AppConstants.listaDone)
                .queryString("idLabels", AppConstants.labelResearch)
                .queryString("idLabels", AppConstants.labelTest)
                .queryString("name", title)
                .queryString("desc", descripcion)
                .asString();
    }
}
